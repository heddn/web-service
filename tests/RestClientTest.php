<?php

declare(strict_types=1);

namespace Soong\ws;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Client\Provider\GenericProvider;
use PHPUnit\Framework\TestCase;

final class RestClientTest extends TestCase
{
    /**
     * Test rest client.
     */
    public function testClient(): void
    {
        // Create a mock and queue two responses.
        $mock = new MockHandler([
            new Response(200, [], \json_encode([
                'access_token' => 'abc123',
                'resource_owner_id' => 1,
            ], JSON_THROW_ON_ERROR)),
            new Response(200, [], \json_encode([
                'success' => true,
            ], JSON_THROW_ON_ERROR)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $httpClient = new Client(['handler' => $handlerStack]);
        $configuration = [
            'authentication_provider' => GenericProvider::class,
            'authentication_provider_options' => [
                'urlAuthorize' => 'https://example.com/authorize',
                'urlAccessToken' => 'https://example.com/token',
                'urlResourceOwnerDetails' => 'https://example.com/resource',
                'username' => 'myuser',
                'password' => 'mysupersecretpassword'
            ],
            'authentication_provider_collaborators' => [
                'httpClient' => $httpClient,
            ],
        ];
        $rest = new RestClient($configuration, $httpClient);
        self::assertInstanceOf(RestClient::class, $rest);
        $response = $rest('https://example.com/rest/endpoint', 'GET', []);
        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals(\json_encode(['success' => true], JSON_THROW_ON_ERROR), (string) $response->getBody());
    }
}
