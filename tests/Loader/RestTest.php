<?php

declare(strict_types=1);

namespace Soong\ws\Loader;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Client\Provider\GenericProvider;
use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Exception\LoaderException;
use Soong\Data\BasicRecord;
use Soong\Data\BasicRecordPayload;
use Soong\Loader\LoaderBase;
use Soong\ws\RestClient;

final class RestTest extends TestCase
{
    /**
     * Test rest loader.
     */
    public function testRestLoader(): void
    {
        $mock = new MockHandler([
            new Response(200, [], \json_encode([
                'access_token' => 'abc123',
                'resource_owner_id' => 1,
            ], JSON_THROW_ON_ERROR)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $httpClient = new Client(['handler' => $handlerStack]);
        $configuration = [
            'key_properties' => [
              'id' => [
                  'type' => 'integer'
              ],
            ],
            'properties' => [
                'name' => [
                    'type' => 'string'
                ],
            ],
            'endpoint' => '/user',
            'endpoint_host' => 'https://example.com',
            'request_method' => 'PUT',
            'authentication_provider' => GenericProvider::class,
            'authentication_provider_options' => [
                'urlAuthorize' => 'https://example.com/authorize',
                'urlAccessToken' => 'https://example.com/token',
                'urlResourceOwnerDetails' => 'https://example.com/resource',
                'username' => 'myuser',
                'password' => 'mysupersecretpassword'
            ],
            'authentication_provider_collaborators' => [
                'httpClient' => $httpClient,
            ],
        ];
        $rest = new TestRestLoader($configuration);
        self::assertInstanceOf(Rest::class, $rest);
        $record = new BasicRecord([
            'id' => 1,
            'name' => 'John',
        ]);
        $payload = new BasicRecordPayload($record, $record);
        $payload = $rest($payload);
        self::assertEquals('abc123', $payload->getDestinationRecord()->getPropertyValue('id'));
    }
}

final class TestRestLoader extends Rest
{

    protected function getRestClient(): RestClient
    {
        $mock = new MockHandler([
            new Response(200, [], \json_encode([
                'success' => true,
            ], JSON_THROW_ON_ERROR)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $httpClient = new Client(['handler' => $handlerStack]);
        return new RestClient(\array_diff_key(
            $this->configuration,
            LoaderBase::optionDefinitions(),
            $this->webServiceOptions()
        ), $httpClient);
    }

    protected function getId(RecordPayload $payload): ?string
    {
        return 'abc123';
    }
}
