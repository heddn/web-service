# soong\web-service

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Soong\web-service provides web service integration with the Soong data migration framework.

## Install

Via Composer

``` bash
$ composer require soong/web-service
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Lucas Hedding][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/web-service.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/web-service/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/web-service.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/web-service.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/web-service.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/web-service
[link-travis]: https://travis-ci.org/soong/web-service
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/web-service/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/web-service
[link-downloads]: https://packagist.org/packages/soong/web-service
[link-author]: https://gitlab.com/heddn
[link-contributors]: ../../contributors
