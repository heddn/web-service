<?php

declare(strict_types=1);

namespace Soong\ws;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use League\OAuth2\Client\Provider\AbstractProvider;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Soong\Configuration\OptionsResolverComponent;

class RestClient extends OptionsResolverComponent implements AuthenticateInterface
{
    use AuthenticationOptionsTrait;

    private ?string $oauth2Token = null;

    public function __construct(
        array $configuration,
        ?ClientInterface $httpClient = null
    ) {
        parent::__construct($configuration);
        if (!\class_exists($this->authenticationProviderClass())) {
            $message = sprintf('Authentication provider (%s) does not exist.', $this->authenticationProviderClass());
            throw new \LogicException($message);
        }

        $this->httpClient = $httpClient ?? new Client();
    }

    public function authenticate(): void
    {
        $class = $this->authenticationProviderClass();
        $provider = new $class($this->authenticationProviderOptions(), $this->authenticationProviderCollaborators());
        \assert($provider instanceof AbstractProvider);
        $this->oauth2Token = $provider->getAccessToken(
            $this->authenticationProviderGrant(),
            $this->authenticationProviderOptions()
        )->getToken();
    }

    public function __invoke(string $uri, string $method = 'GET', array $body = []): ResponseInterface
    {
        if ($this->oauth2Token === null) {
            $this->authenticate();
        }
        $request = new Request(
            $method,
            $uri,
            [
                'Authorization' => sprintf('Bearer %s', $this->oauth2Token),
                'Content-Type' => 'application/json',
            ],
            \json_encode($body, JSON_THROW_ON_ERROR|JSON_UNESCAPED_UNICODE),
        );
        $response = $this->httpClient->send($request, [
            RequestOptions::HTTP_ERRORS => false,
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 401) {
            $this->authenticate();
            $response = $this($uri, $method, $body);
        }
        return $response;
    }

    public function authenticationProviderClass(): string
    {
        return $this->getConfigurationValue('authentication_provider');
    }

    public function authenticationProviderOptions(): array
    {
        return $this->getConfigurationValue('authentication_provider_options');
    }

    public function authenticationProviderCollaborators(): array
    {
        return $this->getConfigurationValue('authentication_provider_collaborators');
    }

    public function authenticationProviderGrant(): string
    {
        return $this->getConfigurationValue('authentication_provider_grant');
    }
}
