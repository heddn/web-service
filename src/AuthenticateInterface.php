<?php

declare(strict_types=1);

namespace Soong\ws;

/**
 * Common authentication implementation for Soong's web service implementations.
 */
interface AuthenticateInterface
{

    /**
     * Provider should implement League\OAuth2\Client\Provider\AbstractProvider.
     *
     * @return string
     */
    public function authenticationProviderClass(): string;

    /**
     * The authentication provider grant type, i.e. password.
     *
     * @return string
     */
    public function authenticationProviderGrant(): string;

    /**
     * The authentication provider options.
     *
     * @return array
     */
    public function authenticationProviderOptions(): array;

    /**
     * The authentication provider collaborators.
     *
     * @return array
     */
    public function authenticationProviderCollaborators(): array;

    /**
     * Authenticate a user.
     *
     * @return void
     */
    public function authenticate(): void;
}
