<?php

declare(strict_types=1);

namespace Soong\ws;

trait WebServiceOptionsTrait
{

    protected function optionDefinitions(): array
    {
        $options = [];
        $options['endpoint'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        $options['endpoint_host'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        $options['request_method'] = [
            'required' => true,
            'allowed_types' => 'string',
            'default_value' => 'GET',
        ];
        return $options;
    }
}
