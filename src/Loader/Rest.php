<?php

declare(strict_types=1);

namespace Soong\ws\Loader;

use Psr\Http\Message\ResponseInterface;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Exception\LoaderException;
use Soong\Loader\LoaderBase;
use Soong\ws\AuthenticationOptionsTrait;
use Soong\ws\RestClient;
use Soong\ws\WebServiceInterface;
use Soong\ws\WebServiceOptionsTrait;

/**
 * Real dumb demo of a simple loader.
 */
class Rest extends LoaderBase implements WebServiceInterface
{

    use AuthenticationOptionsTrait {
        AuthenticationOptionsTrait::optionDefinitions as authenticationOptions;
    }
    use WebServiceOptionsTrait {
        WebServiceOptionsTrait::optionDefinitions as webServiceOptions;
    }

    protected ?RestClient $client = null;

    protected function optionDefinitions(): array
    {
        return parent::optionDefinitions() + $this->authenticationOptions() + $this->webServiceOptions();
    }

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        try {
            $this->execute($payload);
            return $this->setId($payload);
        } catch (\Throwable $throwable) {
            throw new LoaderException('Unable to load.', 0, $throwable);
        }
    }

    protected function execute(RecordPayload $payload): ResponseInterface
    {
        $destinationRecord = $payload->getDestinationRecord();
        $uri = $this->endpointHost() . $this->endpoint();
        return $this->getRestClient()($uri, $this->requestMethod(), $destinationRecord->toArray());
    }

    protected function setId(RecordPayload $payload): RecordPayload
    {
        $destinationRecord = $payload->getDestinationRecord();
        $id = $this->getId($payload);
        if ($id !== null) {
            $keyKeys = array_keys($this->getKeyProperties());
            $destinationRecord->setPropertyValue(reset($keyKeys), $id);
        }
        $payload->setDestinationRecord($destinationRecord);
        return $payload;
    }

    protected function getId(RecordPayload $payload): ?string
    {
        throw new \LogicException('Getting IDs must be implemented.');
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        // @todo not supported.
        throw new LoaderException('Delete is not supported.');
    }

    protected function getRestClient(): RestClient
    {
        if ($this->client === null) {
            try {
                $this->client = new RestClient(\array_diff_key(
                    $this->configuration,
                    parent::optionDefinitions(),
                    $this->webServiceOptions()
                ));
            } catch (\Throwable $throwable) {
                throw new LoaderException('Unable to invoke REST client.', 0, $throwable);
            }
        }
        return $this->client;
    }

    public function endpoint(): string
    {
        return $this->getConfigurationValue('endpoint');
    }

    public function endpointHost(): string
    {
        return $this->getConfigurationValue('endpoint_host');
    }

    public function requestMethod(): string
    {
        return $this->getConfigurationValue('request_method');
    }
}
