<?php

declare(strict_types=1);

namespace Soong\ws;

trait AuthenticationOptionsTrait
{

    protected function optionDefinitions(): array
    {
        $options = [];
        $options['authentication_provider'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        $options['authentication_provider_grant'] = [
            'required' => true,
            'allowed_types' => 'string',
            'default_value' => 'password',
        ];
        $options['authentication_provider_options'] = [
            'required' => false,
            'allowed_types' => 'array',
            'default_value' => [],
        ];
        $options['authentication_provider_collaborators'] = [
            'required' => false,
            'allowed_types' => 'array',
            'default_value' => [],
        ];
        return $options;
    }
}
