<?php

declare(strict_types=1);

namespace Soong\ws;

/**
 * Common implementation for Soong's web service implementations.
 */
interface WebServiceInterface
{

    /**
     * The REST endpoint i.e. /rest/endpoint/here
     * @return string
     */
    public function endpoint(): string;

    /**
     * The REST endpoint host i.e. https://example.com
     * @return string
     */
    public function endpointHost(): string;

    /**
     * The request method, i.e. GET.
     * @return string
     */
    public function requestMethod(): string;
}
